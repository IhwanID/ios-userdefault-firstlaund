//
//  LoginViewController.swift
//  test-user
//
//  Created by Ihwan ID on 16/06/20.
//  Copyright © 2020 Ihwan ID. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func LogginTapped(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "isLoggin")
        performSegue(withIdentifier: "toMain", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
